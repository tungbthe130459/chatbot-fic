Đầu tiên, ta tạo một thư mục để lưu file của project. Để cài đặt Rasa, ta dùng lệnh:
`pip install rasa` 

Để khởi tạo project Rasa, chúng ta chuyển vào thư mục đã tạo ban đầu và gõ lệnh:
`rasa init`

Máy sẽ hỏi chúng ta lưu vào mục nào, nhấn Enter để lưu vào mục hiện tại. Sau đó, Rasa sẽ tạo file và hỏi có train model hay không, gõ Yes(Y) để máy train với dữ liệu mặc định. Sau khi train xong, máy sẽ hỏi có muốn thử chat cùng bot hay không. Quá trình cài đặt đã xong.

Xem trong file vừa tạo, ta sẽ cần lưu ý 5 file:
. domain.yml (file này gồm data về phản hồi của máy)
. endpoints.yml (file này nếu chúng ta xây dựng chatbot có lấy dữ liệu ngoài, truy cập CSDL mà có viết code vào file action.py)
. action.py
. nlu.md (trong thư mục data, file này gồm data về ý đồ của user)
. stories.md (trong thư mục data, file này dùng để kết nối ý đồ của user và máy, trong file bao gồm các kịch bản trò chuyện được định nghĩa)

Tiếp theo, để train và khởi động Rasa chatbot mới, ta gõ lệnh:
`rasa train`
(Lưu ý: ta cần thêm data mới vào hoặc ít nhất một trong các file nlu.md, domain.yml, stories.md nếu không máy sẽ báo 'Nothing changed')

Sau đó, để chat tại localhost, ta gõ lệnh:
`rasa shell`

Tiếp theo, để Rasa có thể chat trên Messenger, ta phải phải có một Fanpage và tài khoản Facebook Developer. Sau khi đăng ký tài khoản Facebook Developer, ta sẽ được chuyển sang cửa sổ mới, chọn Messenger và chọn Setup. Tiếp theo, trên màn hình mới, ta thấy phần Access Token và bấm “Add Remove Pages” để chọn cái Fanpage mình vừa tạo ở trên.

Đến phần cấu hình Facebook Messenger vào Rasa:
- Bước một, lấy Access Token bằng cách nhấn vào nút Generate Token ở hình trên và sau đó nhấn tick.
- Bước hai, đến bước lấy App Secret. Bấm vào menu trái, chọn Settings, chọn Basic. Bên ô App Secret, nhấn Show để lấy mã.
- Bước ba, ta tự tạo một đoạn mã bất kỳ để làm mã kiểm tra cho webhook.

Sau khi đã có cả 3 thông tin trên, ta vào thư mục rasa và mở file credentials.yml, uncomment phần facebook và tiền vào thông tin như sau:

`facebook:`
  
  `verify: "<Điền đoạn mã bất kỳ mà bạn đã tạo ở bước 3>"`
  
  `secret: "<Điền cái mã tại bước 2>"`
  
  `page-access-token: "<Điền mã ở bước 1>"`

Để chạy Server Rasa tại máy tính cá nhân, ta cần mở cổng tại địa chỉ: http://localhost:5005. Sau đó, ta vào trang https://ngrok.com/ để tải phần mềm theo hệ điều hành.

Start Server Rasa tại localhost bằng lệnh:
`rasa run --endpoints endpoints.yml --credentials credentials.yml` 

Tiếp theo, chuyển vào thư mục chứa file ngrok và chạy lệnh:
`ngrok http 5005`

Sau khi chạy lệnh trên, ngrok sẽ cho ta địa chỉ HTTPS. Mở trình duyệt lên, gõ địa chỉ HTTPS mà ngrok cho để mapping 

Trên giao diện của Facebook Developer, chọn Facebook App mà ta đã tạo từ đầu, chọn Settings, đến phần Webhook và chọn Add Callback URL. Nhập vào địa chỉ ngrok và thêm đuôi `/webhooks/facebook/webhook`, bên dưới thì nhập đoạn mã kiểm tra mà ta đã tự tạo ra ở trên tại phần verify và nhấn Verity and Save. Sau khi add xong Webhook thì chọn nút Edit và tick vào messages rồi save lại.

Sau khi làm hết tất cả các bước trên, ta có thể test chatbot Rasa trên chính Fanpage mà ta đã tạo.







